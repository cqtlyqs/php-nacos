<?php

use Ktnw\nacos\NacosUtils;
/**
 * 发送心跳示例
 * 这里简单的采用循环实现定时任务，可根据实际情况，自己实现定时任务。
 */
require __DIR__.'/../vendor/autoload.php';

try {
    // 发送心跳
    NacosUtils::beat();
    $time = 14;
    sleep($time);
    $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    var_dump($url);
    file_get_contents($url);
} catch (Exception $e) {
    print_r($e->getMessage());
}