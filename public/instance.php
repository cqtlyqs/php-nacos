<?php

use Ktnw\nacos\NacosUtils;

require __DIR__ . '/../vendor/autoload.php';

try {
    var_dump(NacosUtils::getInstance());
} catch (Exception $e) {
    print_r($e->getMessage());
}