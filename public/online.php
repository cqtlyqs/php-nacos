<?php

use Ktnw\nacos\NacosUtils;

require __DIR__ . '/../vendor/autoload.php';

try {
    $r = NacosUtils::serverOnline();
    var_dump($r);
} catch (Exception $e) {
    print_r($e->getMessage());
}