<?php
/**
 * 自动加载的入口文件
 * Created by PhpStorm.
 * User: 86157
 * Date: 2021/12/30
 * Time: 15:43
 */


use Illuminate\Support\Facades\Log;
use Ktnw\nacos\NacosUtils;

require __DIR__.'/../vendor/autoload.php';

try {
   if (NacosUtils::register()) {
       print_r("已注册到Nacos");
   }else{
       print_r("注册失败");
   }
} catch (Exception $e) {
    var_dump($e->getMessage());
    // Log::error($e->getMessage());
}