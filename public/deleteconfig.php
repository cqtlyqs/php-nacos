<?php

use Ktnw\nacos\NacosUtils;

require __DIR__ . '/../vendor/autoload.php';

try {
    $r = NacosUtils::deleteConfig("phpTool-up-down.text", "DEFAULT_GROUP");
    var_dump($r);
} catch (Exception $e) {
    print_r($e->getMessage());
}