# php-nacos

#### 介绍
php集成nacos OPEN API

#### 软件架构
软件架构说明


#### 安装教程

1. composer引入: composer require ktnw/nacos;
2. 发布: php artisan vendor:publish --provider="Ktnw\nacos\Providers\RepositoryServiceProvider"
3. 在全局中间件中添加NacosRegister中间件。
   即是，App\Http\Kernel中，protected $middleware中添加:
   protected $middleware = [
   ...
   \App\Http\Middleware\NacosRegister::class,
   ...
  ]。
4. 注册到Nacos。向服务器发送一次http请求后，会自动注册到Nacos中. 紧接着向服务器端发送心跳。
   注意: 注册到Nacos后，只有15秒的有效期。需在有效期内，向Nacos发送心跳。Nacos中的实例才不会下线。
5. 发送心跳到Nacos。
   这里有两种方式：1. 采用定时任务，每隔12秒向nacos发送一次心跳。2. 访问public目录中的nacosBeat.php文件。
   建议选择方式1向Nacos发送心跳.


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

#### 集成功能:
1. 读取Nacos配置文件
2. Nacos登录
3. Nacos注册
4. 获取nacos中实例列表 && 获取nacos中实例详情
5. 当前实例是否已注册到Nacos中
6. 获取Nacos中实例URL(使用实例URL+路由可调用注册到Nacos中的任何实例)
7. 发送心跳
8. 服务下线
9. 服务上线
10. 其他功能 待集成


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
