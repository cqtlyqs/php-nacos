<?php

use Ktnw\nacos\NacosApplication;
use Ktnw\nacos\Config;



/**
 * 获取配置文件的属性
 * @param string name
 */
function getConfig(string $name)
{
    $config = NacosApplication::getInstance(Config::class);
    return $config->get($name);
}


