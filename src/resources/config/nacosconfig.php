<?php
return [
    "serverAddress"      => 'http://172.127.10.229:8849', // nacos地址
    "auth"               => true, // 是否启用nacos授权
    "userName"           => 'nacos',// 账号
    "password"           => 'nacos',// 密码
    "serverName"         => 'phpTool',// serverName
    "gatewayServerName"  => 'zxzd-gateway', // Nacos中网关服务的名称，若未使用网关，可配置为空.
    "accessServerPrefix" => "http", //访问Nacos中实例的前缀
];