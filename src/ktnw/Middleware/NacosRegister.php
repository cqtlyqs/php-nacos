<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ktnw\nacos\NacosUtils;
use Exception;

/**
 * 注册到Nacos
 */
class NacosRegister
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        try {
            NacosUtils::register();
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
        return $response;
    }
}
