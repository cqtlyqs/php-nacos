<?php

namespace Ktnw\nacos\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../resources/config/nacosconfig.php' => config_path('nacosconfig.php'),
            __DIR__ . '/../Middleware/NacosRegister.php' => app_path("Http/Middleware/NacosRegister.php"),
            __DIR__ . '/../../../public/nacosBeat.php' => public_path("nacosBeat.php"),
        ]);
        $this->mergeConfigFrom(__DIR__ . '/../../resources/config/nacosconfig.php', 'nacosconfig');
    }

}