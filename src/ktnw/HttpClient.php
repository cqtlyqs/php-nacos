<?php

namespace Ktnw\nacos;

/**
 * http操作封装类
 */
class HttpClient
{
    /**
     * [http 调用接口函数]
     * @Date   2020-07-14
     * @param string $url [接口地址]
     * @param array $params [数组]
     * @param string $method [GET\POST\DELETE\PUT]
     * @param array $header [HTTP头信息]
     * @param integer $timeout [超时时间]
     * @return array|string [type] [接口返回数据]
     */
    public static function sendHttp(string $url, array $params = [], string $method = 'GET', array $header = [], int $timeout = 10)
    {
        $headers = [
            $method == "GET" ? "Content-Type: application/json;charset=utf-8" : "Content-Type: application/x-www-form-urlencoded; charset=utf-8",
        ];
        if (!empty($headers)) {
            $header = array_merge($headers, $header);
        }

        /* 初始化并执行curl请求 */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        if (strtoupper(substr($url, 0, 5)) == 'HTTPS') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        /* 根据请求类型设置特定参数 */
        switch (strtoupper($method)) {
            case 'GET':
                $params = empty($params) ? [] : $params;
                if (is_array($params)) {
                    $params = http_build_query($params);
                }
                if (strpos($url, "?") === false) {
                    $url .= '?&' . $params;
                } else {
                    $url .= '&' . $params;
                }
                curl_setopt($ch, CURLOPT_URL, $url);
                break;
            case 'POST':
                $params = empty($params) ? [] : $params;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-HTTP-Method-Override: DELETE"]);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            case 'PUT':
                $params = empty($params) ? [] : $params;
                if (is_array($params)) {
                    $params = http_build_query($params);
                }
                if (strpos($url, "?") === false) {
                    $url .= '?&' . $params;
                } else {
                    $url .= '&' . $params;
                }
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            default:
                return [];
        }
        $data = curl_exec($ch);
        //关闭URL请求
        curl_close($ch);
        if (empty($data)) {
            return [];
        }
        if (strpos($data, "{") === false) {
            return $data;
        }
        return json_decode($data, true);
    }

    /**
     * 对象转换数组
     * @param $obj
     * @return array
     */
    private static function object_to_array($obj): array
    {

        if (!is_array($obj) && !is_object($obj)) {
            return $obj;
        }
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        $arr  = [];
        foreach ($_arr as $key => $val) {
            $val       = (is_array($val)) || is_object($val) ? self::object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

}