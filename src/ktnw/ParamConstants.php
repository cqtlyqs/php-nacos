<?php

namespace Ktnw\nacos;
/**
 * Nacos openAPI 的参数常量类
 */
class ParamConstants
{
    /**
     * 服务名
     */
    const SERVICE_NAME = "serviceName";

    /**
     * 服务实例port
     */
    const PORT = "port";

    /**
     * 命名空间ID
     */
    const NAMESPACE_ID = "namespaceId";

    /**
     * 命名空间ID
     */
    const GROUP_NAME = "groupName";

    /**
     * 集群名
     */
    const CLUSTER_NAME = "clusterName";

    /**
     * 权重
     */
    const WEIGHT = "weight";

    /**
     * 鉴权TOKEN名称
     */
    const ACCESS_TOKEN = "accessToken";

    /**
     * ip
     */
    const IP = "ip";

    /**
     * nacos登录用户名
     */
    const USERNAME = "username";

    /**
     * nacos登录密码
     */
    const PASSWORD = "password";

    /**
     * token有效期
     */
    const TOKEN_TTL = "tokenTtl";

    /**
     * token创建时间
     */
    const TOKEN_CREATE_TIME = "tokenCreateTime";

    /**
     * JSON格式字符串, 实例心跳内容
     */
    const BEAT = "beat";

    /**
     *
     */
    const CLUSTER = "cluster";

    /**
     * 是否只返回健康实例
     */
    const HEALTHY_ONLY = "healthyOnly";

    /**
     * 配置 ID
     */
    const DATA_ID = "dataId";

    /**
     * 配置分组
     */
    const DATA_GROUP = "group";

    /**
     * 配置内容
     */
    const DATA_CONTENT = "content";

}