<?php

namespace Ktnw\nacos;

class NacosApplication
{

    static $instance = [];

    /**
     * 绑定实例
     * @param string abstract
     * @param $instance
     *
     */
    public static function instance(string $abstract, $instance)
    {
        self::$instance[$abstract] = $instance;
    }

    /**
     * 获取实例
     * @param string $abstract
     * @return mixed
     */
    public static function getInstance(string $abstract)
    {
        $instance = self::$instance[$abstract];
        if (is_null($instance)) {
            // print_r("初始化".$abstract)
            $instance = new $abstract;
            self::instance($abstract, $instance);
        }
        return $instance;
    }


}