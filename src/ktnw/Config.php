<?php

namespace Ktnw\nacos;

use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem;

/**
 * 加载配置文件
 */
class Config extends Repository
{

    public function __construct()
    {
        parent::__construct();
        $this->loadConfigResource();
    }

    /**
     * @param string path
     */
    private function loadConfigResource()
    {
        $path       = __DIR__.'/../../src/resources/config';
        $filesystem = new Filesystem();
        if (!$filesystem->isDirectory($path)) {
            return;
        }
        foreach ($filesystem->allFiles($path) as $file) {
            $relativePathname = $file->getRelativePathname();
            $pathInfo         = pathinfo($relativePathname);
            if ($pathInfo['dirname'] == '.') {
                $key = $pathInfo['filename'];
            } else {
                $key = str_replace('/', '.', $pathInfo['dirname']) . '.' . $pathInfo['filename'];
            }
            $this->set($key, require $path . '/' . $relativePathname);
        }
    }
}